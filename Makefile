DOCKER := docker
TAG := chat.mongotel.com:latest
IMAGE := chat.mongotel.com
NAME := $(shell head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13 ; echo '')

build-image:
	sudo ${DOCKER} build --build-arg NPM_TOKEN=${NPM_ACCESS_TOKEN} --build-arg API=${API} . -t ${TAG} || /bin/false

build-folder:
	sudo mkdir -p $$(pwd)/build || /bin/false

get-build:
	sudo ${DOCKER} run -d --name ${NAME} ${IMAGE} || /bin/false
	sudo ${DOCKER} cp ${NAME}:/app/public $$(pwd)/build || /bin/false
	sudo ${DOCKER} kill ${NAME} || /bin/false

get-name:
	@echo ${NAME} || /bin/false