import React from 'react';
import {Provider} from 'react-redux';
import { ToastContainer } from 'react-toastify';
import { UX } from '@liontechnyc/gemini';
import { storeFactory} from './src/redux';
import './src/styles/app.scss';

export const wrapRootElement = ({ element, props }) => {
  const appStore = storeFactory();
  return (
    <Provider store={appStore}>
        {element}
    </Provider>
  );
};

export const wrapPageElement = ({ element, props }) => {
  <UX.Provider ssr>{element}</UX.Provider>;
};

export const onRenderBody = ({ setHeadComponents, setPreBodyComponents }, pluginOptions) => {
  setHeadComponents([
    <link 
      rel="dns-prefetch" 
      href="//kit.fontawesome.com"
      key="dns-fontawesome"
    />,
    <link 
      rel="dns-prefetch" 
      href="//cdn.jsdelivr.net"
      key="dns-jsdelivr"
    />,
    <script
      src="https://kit.fontawesome.com/5ff6aed892.js"
      crossOrigin="anonymous"
      key="fontawesome"
    />,
    <script
      src="https://code.jquery.com/jquery-3.6.0.min.js"
      integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
      crossOrigin="anonymous"
      key="jquery"
    />,
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/animate.css@4.1.0/animate.css"
      integrity="sha256-6Wozb/CzRANFOaGqN+u/uRT7kcffVIQt9OhOuuA6I9o="
      crossOrigin="anonymous"
      key="animate-css"
    />,
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/react-toastify@6.0.8/dist/ReactToastify.css"
      integrity="sha256-RxKz0HTc9uwO5Fdh8T73LX0ho6XnKGHJQ+HfqoB50Io="
      crossOrigin="anonymous"
      key="react-toastify-css"
    />,
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/emoji-mart@3.0.0/css/emoji-mart.css"
      integrity="sha256-l2TVdn0omebJaWAf9pSlP5ScEl5KDfc8jFIpdoCZLCg="
      crossOrigin="anonymous"
      key="emoji-mart-css"
    />,
    /* Inject @artsy/fresnel styles in to the head */
    <style key="ux-media-styles">
      {UX.mediaStyle}
    </style>
  ]);
  setPreBodyComponents([
    <ToastContainer
        key="toast-container"
        position="top-right"
        autoClose={1500}
        draggable
        draggablePercent={60}
        draggableDirection="x"
        closeOnClick
        pauseOnHover
      />
  ]);
};
