import React from 'react';
import {Provider} from 'react-redux';
import { UX } from '@liontechnyc/gemini';
import { storeFactory} from './src/redux';
import './src/styles/app.scss';

export const wrapRootElement = ({ element, props }) => {
  const appStore = storeFactory();
  return (
    <Provider store={appStore}>
        {element}
    </Provider>
  );
};

export const wrapPageElement = ({ element, props }) => {
  return(
    <UX.Provider>
      {element}
    </UX.Provider>
  )
}