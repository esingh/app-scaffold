FROM node:10.23-alpine3.10 AS Installer

WORKDIR /app

COPY package*.json ./

ARG API=development

ARG NPM_TOKEN

ENV API_ENV=${API}

RUN npm config set '//registry.npmjs.org/:_authToken' "${NPM_TOKEN}"

RUN npm ci

COPY . .

FROM Installer AS UnitTester

RUN npm run test

FROM Installer AS Bundler

RUN npm run clean
RUN npm run build

CMD ["sh", "-c", "tail -f /dev/null"]
