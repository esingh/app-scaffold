const path = require(`path`);
const webpack = require('webpack');

const templates = {
  policyTemplate: path.resolve(`src/templates/policyTemplate.tsx`),
};

async function generateRedirects({ actions }){
  const { createRedirect } = actions

    createRedirect({
     fromPath: `/`,
     toPath: `/login/`,
     redirectInBrowser: true,
     isPermanent: true,
    });
}

exports.createPages = async ({ actions, graphql, reporter }) => {
  //await generateMarkdownTemplates({ actions, graphql, reporter });
  await generateRedirects({ actions });
};

exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions;
  if (page.path.match(/^\/app/)) {
    page.matchPath = '/app/*';
    createPage(page);
  }
};

exports.onCreateWebpackConfig = ({ loaders, stage, actions }) => {
  if (stage === 'build-html') {
    actions.setWebpackConfig({
      resolve: {
        fallback: {
          fs: false,
          tls: false,
          child_process: false,
          net: false,
          https: require.resolve("https-browserify"),
          http: require.resolve("stream-http"),
          crypto: require.resolve("crypto-browserify")
        }
      },
      module: {
        rules: [
          {
            test: /canvas/,
            use: loaders.null(),
          },
        ],
      },
      plugins: [
        new webpack.ProvidePlugin({
          document: 'min-document',
          self: 'node-noop',
          'self.navigator.userAgent': 'empty-string',
          window: 'node-noop',
        }),
      ],
      externals: {
        $: 'jQuery',
      }
    });
  }
};
