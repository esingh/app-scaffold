import { ActionHandler, defaultState } from '../../interfaces';
import { merge } from '../../utils';

export const storeProfile: ActionHandler = (state = defaultState, action) => {
  const { user } = action.payload;
  return merge(state)({ user });
};

export const logout: ActionHandler = (state = defaultState, action) => {
  return defaultState;
};

export const apiFailure: ActionHandler = (state = defaultState, action) => {
  const { msg, err } = action.payload;
  console.warn(msg);
  console.error(err);
  return state;
};
