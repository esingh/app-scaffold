import { AppReducer } from '../../interfaces';
import { CoreActions, type } from '../actions';
import { storeProfile, logout, apiFailure } from './core';

const stub = () => {};

export default <AppReducer>{
  [type(CoreActions.STORE_PROFILE)]: storeProfile,
  [type(CoreActions.LOGOUT)]: logout,
  [type(CoreActions.API_FAILURE)]: apiFailure,
};
