import compose from 'lodash/fp/compose';
import { toast, TypeOptions as AlertTypes } from 'react-toastify';
import { clear } from 'redux-localstorage-simple';
import {client} from '../../api';
import {reduceApiResults} from '../../utils';
import {ActionType} from '../../interfaces';
import { CoreActions, type } from './constants';

export function initApp(){
  return dispatch => {
    if(client.accessToken){
      // ? Load profile
      compose(dispatch, loadProfile)();
    } else {
      // ? Clear app state
      compose(dispatch, logout)();
    }
  }
}

export function storeProfile(userProfile, tenantProfile, users) {
  return <ActionType>{
    type: type(CoreActions.STORE_PROFILE),
    payload: {
      userProfile,
      tenantProfile,
      users,
    },
  };
}

export function loadProfile() {
  const authEndpoint = client.query('auth', 'GET');
  return async dispatch => {
    try {
      const res = await authEndpoint('me', {params: {}});
      const {ok, data} = reduceApiResults(res);
      if (ok) {
        const {userProfile, tenantProfile, users} = data;
        dispatch(storeProfile(userProfile, tenantProfile, users));
      } else {
        throw new Error();
      }
    } catch (err) {
      dispatch(
        actionFailed(
          CoreActions.API_FAILURE,
          'User profile request failed',
          err,
        ),
      );
    }
  };
}

export function login(email: string, password: string) {
  const authEndpoint = client.query('auth', 'POST');
  return async dispatch => {
    try {
      const res = await authEndpoint('login', {data: {email, password}});
      const {
        ok,
        data,
      } = reduceApiResults(res);
      if (ok && data) {
        client.accessToken = data.token;
        compose(dispatch, loadProfile)();
      }
      else {
        compose(dispatch, uiAlert)('error', 'Invalid credentials')
        compose(dispatch, actionFailed)(
          CoreActions.LOGIN_FAILURE,
          'Invalid credentials',
        );
      }
    } catch (err) {
      compose(dispatch, uiAlert)('error', 'Invalid credentials')
      compose(dispatch, actionFailed)(
        CoreActions.API_FAILURE,
        'Login request failed',
        err,
      );
    }
  };
}

export function logout() {
  return dispatch => {
    client.logout();
    clear({ namespace: 'app_state' });
    dispatch({ type: type(CoreActions.LOGOUT) });
  };
}


export function uiAlert(alertType: AlertTypes = 'info', msg: string){
  return dispatch => {
    toast(msg, { 
      type: alertType, 
      toastId: Buffer.from(msg).toString('base64') 
    });
    dispatch({
      type: type(CoreActions.UI_ALERT),
      payload: { alertType, msg }
    });
}
}

export function actionFailed(
  t: CoreActions,
  msg: string,
  err = null,
): ActionType {
  return {
    type: type(t),
    payload: {msg, err: err && err.toString()},
  };
}
