
export enum CoreActions {
    LOG_IN,
    LOGOUT,
    LOAD_PROFILE,
    STORE_PROFILE,
    API_FAILURE,
    NOTIFICATION
}

/** ? Extract type as string from Typescript enum */
export const type = (t: CoreActions) => CoreActions[t];