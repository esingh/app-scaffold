import { combineReducers, configureStore } from '@reduxjs/toolkit';
import createSocketIoMiddleware from 'redux-socket.io';
import logger from 'redux-logger';
import { save, load } from 'redux-localstorage-simple';
import ExecutionEnvironment from 'exenv';
import coreReducer from './reducer';
import { client } from '../api';
import { defaultState } from '../interfaces';

export const storeFactory = (appReducers = {}, appMiddlewares = []) => configureStore({
    reducer: combineReducers({ core: coreReducer, ...appReducers }),
    //enhancers: [batchedSubscribe(debounce((notify: any) => notify()))],
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat([
        logger,
        save({
            namespace: 'app_state',
            debounce: 500
        }),
        createSocketIoMiddleware(
            client.socket,
            (_type, action) => !!action.streamEvent
        ),
        ...appMiddlewares
    ]),
    devTools: process.env.NODE_ENV !== 'production',
    preloadedState: ExecutionEnvironment.canUseDOM ? load({
        preloadedState: {
            core: defaultState
        }
    }) : { core: defaultState }
});