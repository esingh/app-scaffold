import React from 'react';
import { Collection } from '@liontechnyc/gemini';
import { CollectionDirection } from '@liontechnyc/gemini/build/Collection/Collection';
import _keys from 'lodash/keys';

export const mapSocialIcons: { [media: string]: string } = {
  facebook: 'fab fa-facebook-square',
  instagram: 'fab fa-instagram',
  linkedin: 'fab fa-linkedin',
  twitter: 'fab fa-twitter',
  github: 'fab fa-github',
  google: 'fab fa-google-plus-g',
};

export interface SocialMediaProps {
  align?: CollectionDirection;
  social: {
    [media: string]: string;
  };
}

const SocialMedia = ({ social, align }: SocialMediaProps) => {
  const socialContent = _keys(social).map((mediaChannel, id) => (
    <li key={id}>
      <a href={social[mediaChannel]}>
        <i className={mapSocialIcons[mediaChannel]} />
      </a>
    </li>
  ));

  return (
    <Collection
      containerClass="social__container"
      direction={align || 'horizontal'}
      renderAs="ul"
      align={{ horizontal: 'space-between', vertical: 'center' }}
      items={socialContent}
    />
  );
};

export default SocialMedia;
