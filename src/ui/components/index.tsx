export { default as Loading } from './Loading';
export { default as ErrorMsg } from './ErrorMsg';
export { default as SEO } from './SEO';
export { default as Hero } from './Hero';
export { default as Footer } from './Footer';
