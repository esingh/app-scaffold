import React from 'react';
import Loader from 'react-loader-spinner';
import { Block, Content } from '@liontechnyc/gemini';

export interface LoadingProps {
  title?: string;
  message?: string;
}

const Loading = ({ title, message }: LoadingProps) => {
  return (
    <Block fluid={true} centered={true}>
      <Content
        title={title || 'Loading...'}
        subtitle={message}
        alignTitle="center"
        alignContent="center"
        containerClass="animate__animated animate__fadeIn"
      >
        <br />
        <Loader type="ThreeDots" color="#727cf5" height={80} width={80} />
      </Content>
    </Block>
  );
};

export default Loading;
