import React from 'react';
import { Block, Content } from '@liontechnyc/gemini';

export interface ErrorMsgProps {
  error?: Error;
  children?: React.ReactNode;
}

const ErrorMsg = ({ error, children }: ErrorMsgProps) => {
  console.error({ error });
  return (
    <Block fluid={true} centered={true}>
      <Content title="Errors occured check the console" {...{ children }} />
    </Block>
  );
};

export default ErrorMsg;
