import React from 'react';
import { Block, Collection, Content } from '@liontechnyc/gemini';
import SocialMedia from './SocialMedia';

export interface FooterProps {
  company: string;
  header: string;
  logo: string;
  contact: {
    address: string;
    phone: string;
    email: string;
  };
  social: any;
}

const Footer = ({ company, header, logo, contact, social }: FooterProps) => {
  return (
    <Block
      className="footer"
      renderAs="footer"
      style={{
        justifyContent: 'space-between',
      }}
      inline={true}
      fluid={true}
    >
      <SocialMedia {...{ social }} />
      <Content title="Hello" />
    </Block>
  );
};

export default Footer;
