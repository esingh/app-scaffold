export type ID = number | string;

// ? Application Context

export interface AuthCtx {
  isLoggedIn: boolean;
  isStaff: boolean;
  lastLogin: string | null;
}

export interface UserCtx {
  userId?: ID | null;
  profile?: {
    name: [string, string];
    email: string;
    phoneNumber: string;
  } | null;
}


export interface CoreCtx {
  auth: AuthCtx;
  user: UserCtx;
}

export interface ActionType {
  type: string;
  payload?: any;
  streamEvent?: boolean;
}

export const defaultState: CoreCtx = {
  auth: {
    isLoggedIn: false,
    isStaff: false,
    lastLogin: null,
  },
  user: {
    userId: null,
    profile: null
  },
};

export type ActionHandler = (
  state: CoreCtx | any,
  action: ActionType,
) => CoreCtx | any;

export type AppReducer = {[action: string]: ActionHandler};
