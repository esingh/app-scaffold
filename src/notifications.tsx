import { ActionType } from './interfaces';

const notificationsMiddleware =
  (store: any) => (next: any) => (action: ActionType) => {
    if (action.type === 'NOTIFICATION') {
      const state = store.getState();
    }
    return next(action);
  };

export { notificationsMiddleware };
