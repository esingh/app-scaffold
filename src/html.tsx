import React from 'react';
import './styles/app.scss';

interface HtmlProps {
  htmlAttributes: object;
  headComponents: [];
  bodyAttributes: object;
  preBodyComponents: [];
  body: string;
  postBodyComponents: [];
}

const HTML = (props: HtmlProps) => {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        {props.headComponents}
        <script
          type="text/javascript"
          dangerouslySetInnerHTML={{
            __html: `
            window.ATL_JQ_PAGE_PROPS = $.extend(window.ATL_JQ_PAGE_PROPS, {
              "e8cb8929": {
                "triggerFunction": function(showCollectorDialog) {
                  window.__e8cb8929__showCollectorDialog = showCollectorDialog;
                },
              },
              "9f09f6e8": {
                "triggerFunction": function(showCollectorDialog){
                  window.__9f09f6e8__showCollectorDialog = showCollectorDialog;
                },
              },
              "3536885a": {
                "triggerFunction": function(showCollectorDialog) {
                  window.__3536885a__showCollectorDialog = showCollectorDialog;
                },
              },
            });
            `,
          }}
        />
      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}
      </body>
    </html>
  );
};

export default HTML;
