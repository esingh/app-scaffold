import { graphql } from 'gatsby';

export const ErrorContentFragment = graphql`
  fragment ErrorContentFragment on ErrorYamlConnection {
    edges {
      node {
        content {
          title
          description
        }
      }
    }
  }
`;
