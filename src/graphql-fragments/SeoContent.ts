import { graphql } from 'gatsby';

export const SeoMetaFragment = graphql`
  fragment SeoMetaFragment on SeoYamlConnection {
    edges {
      node {
        meta {
          title
          description
          keywords
          author
          publicUrl
          contact {
            address
            email
            phone
          }
          social {
            facebook
            instagram
            twitter
          }
        }
      }
    }
  }
`;
