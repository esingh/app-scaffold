import { graphql } from 'gatsby';

export const LoginContentFragment = graphql`
  fragment LoginContentFragment on LoginYamlConnection {
    edges {
      node {
        content {
          title
          description
        }
      }
    }
  }
`;
