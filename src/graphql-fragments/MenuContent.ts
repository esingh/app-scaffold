import { graphql } from 'gatsby';

export const MenuContentFragment = graphql`
  fragment MenuContentFragment on MenuYamlConnection {
    edges {
      node {
        category {
          name
          links {
            title
            route
          }
        }
      }
    }
  }
`;
