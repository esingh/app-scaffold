import axios, {
  AxiosInstance,
  Method,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import * as io from 'socket.io-client';
import SocketMock from 'socket.io-mock';
import ExecutionEnvironment from 'exenv';
import { toast } from 'react-toastify';

type ApiEndpoint = 'auth' | 'media';

/** Provides an injectable interface to the webservice API client */
export class WebServiceClient {
  private static readonly localStorageKey = 'accessToken';
  private static readonly localApiEndpoint = 'http://localhost:8000';
  private static readonly localEventsEndpoint = 'http://localhost:8005';
  private static readonly devApiEndpoint =
    'https://web-api.dev.us-east-1.app.com';
  private static readonly devEventsEndpoint =
    'https://events-api.dev.us-east-1.app.com';
  private static readonly prodApiEndpoint =
    'https://web-api.prod.us-east-1.app.com';
  private static readonly prodEventsEndpoint =
    'https://events-api.prod.us-east-1.app.com';
  public readonly socket;
  private static readonly restApi: AxiosInstance = axios.create({
    baseURL:
      process.env.API_ENV === 'production'
        ? WebServiceClient.prodApiEndpoint
        : process.env.API_ENV === 'development'
        ? WebServiceClient.devApiEndpoint
        : WebServiceClient.localApiEndpoint,
    withCredentials: false,
  });
  public readonly endpoints: { [resource: string]: string } = {
    auth: '/auth',
    media: '/media',
  };

  constructor() {
    // ? Only initialize socket connection on client-side
    if (ExecutionEnvironment.canUseDOM) {
      this.socket = io.connect(
        (process.env.API_ENV === 'production'
          ? WebServiceClient.prodEventsEndpoint
          : process.env.API_ENV === 'development'
          ? WebServiceClient.devEventsEndpoint
          : WebServiceClient.localEventsEndpoint) + '/stream',
        {
          transports:
            process.env.API_ENV === 'production'
              ? ['websocket']
              : ['websocket', 'polling'],
          query: { token: this.accessToken },
        }
      );
      this.registerListeners();
    } else {
      this.socket = new SocketMock();
    }
  }

  private registerListeners() {
    this.socket.on('connect', () => {
      console.log('Events stream online!');
      console.log(`Your events stream ID: ${this.socket.id}`);
    });
    this.socket.on('connect_error', () => {
      console.error(
        'There were issues reconnecting to the Events stream, please reload'
      );
      console.log('Attempting to reconnect manually...');
      this.socket.io.opts.query = { token: this.accessToken };
      this.socket.connect();
    });
    this.socket.on('disconnect', (reason) => {
      console.warn('Disconnected from the Events stream', { reason });
      if (reason === 'io server disconnect') {
        // ? Server forcefully disconnected the client
        toast.info(`Attempting to reconnect... 🔄`);
        this.socket.io.opts.query = { token: this.accessToken };
        this.socket.connect();
      } else if (reason !== 'io client disconnect') {
        // ? Client disconnected by the application
        toast.error(`Events Offline ❌`);
      }
    });
    this.socket.on('reconnect_attempt', () => {
      console.log('Reconnecting to the Events stream...');
      toast.info(`Attempting to reconnect... 🔄`);
      if (process.env.API_ENV !== 'production') {
        // ? on reconnect fall back to polling
        this.socket.io.opts.transports = ['polling', 'websocket'];
      }
    });
    this.socket.on('reconnect', () => {
      console.log('Events stream back online!');
      console.log(`Your events stream ID: ${this.socket.id}`);
      toast.success(`Events Online ✅`);
    });
    window.addEventListener('offline', (e) => {
      toast.error(`You are Offline`);
    });
    window.addEventListener('online', () => {
      toast.success(`Back Online`);
    });
  }

  /** Request resource from Commission API endpoint */
  query(
    resource: ApiEndpoint,
    action: Method,
    hasFiles = false
  ): (
    accessor?: string,
    options?: AxiosRequestConfig
  ) => Promise<AxiosResponse<any>> {
    if (resource in this.endpoints) {
      return (
        accessor = '',
        options?: AxiosRequestConfig
      ): Promise<AxiosResponse<any>> => {
        return WebServiceClient.restApi.request({
          url: this.endpoints[resource] + (accessor ? '/' + accessor : ''),
          method: action,
          headers: {
            authorization: this.accessToken ? `Bearer ${this.accessToken}` : '',
            'Content-Type': hasFiles
              ? 'multipart/form-data'
              : 'application/json',
          },
          ...options,
        });
      };
    } else {
      throw new Error(`"${resource}" is not a API endpoint`);
    }
  }

  // ? Getter & Setter methods for interfacing JWT from localStorage

  get accessToken(): string {
    if (ExecutionEnvironment.canUseDOM) {
      return localStorage.getItem(WebServiceClient.localStorageKey) || '';
    } else {
      return '';
    }
  }

  set accessToken(token: string) {
    if (ExecutionEnvironment.canUseDOM) {
      localStorage.setItem(WebServiceClient.localStorageKey, token);
      this.socket.io.opts.query = { token };
      if (!this.socket.connected) {
        this.socket.connect();
      }
    }
  }

  logout(): void {
    if (ExecutionEnvironment.canUseDOM) {
      localStorage.removeItem(WebServiceClient.localStorageKey);
      this.socket.io.opts.query = {};
      this.socket.disconnect();
    }
  }
}

// ? Singleton instance for app use
const webServiceClient = new WebServiceClient();

export default webServiceClient;
