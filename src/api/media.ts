import client from './client';
import { reduceApiResults } from '../utils';

const uploadMediaQuery = client.query('media', 'POST');

/**
 * Uploads user's content to our CDN
 * @param rawMedia 
 * @returns URI of the S3 object
 */
export async function uploadMedia(rawMedia: any): Promise<string[]> {
  const form = new FormData();
  form.append('media', rawMedia);
  const result = await uploadMediaQuery('', {
    data: form,
    headers: {
      'content-type': 'multipart/form-data',
      Authorization: `Bearer ${client.accessToken}`,
    },
  });
  const { data } = reduceApiResults(result);
  return data.media;
}