import React, { FunctionComponent } from 'react';
import { connect } from 'react-redux';
import { graphql, Link } from 'gatsby';
import { Page, Block, Content } from '@liontechnyc/gemini';
import { placeholder, reduceGqlConnection } from '../utils';

export interface ErrorPageProps {}

const ErrorPage: FunctionComponent<StaticPageProps & ErrorPageProps> = ({
  data: { errorContent },
}) => {
  const [{ content }] = reduceGqlConnection(errorContent);

  return (
    <Page>
      <Block
        fluid={true}
        centered={true}
        style={{
          background: `url(${placeholder(128, 128)})`,
          width: '100vw',
          height: '100vh',
        }}
      >
        <Content
          containerClass="error__content--container"
          contentClass="error__content--body"
          title={content.title}
          subtitle={content.description}
          alignContent="center"
          alignTitle="center"
        >
          <Link className="error__content--btn" to="/logout/">
            Return To Login
          </Link>
        </Content>
      </Block>
    </Page>
  );
};

export const pageContent = graphql`
  query ErrorPageContent {
    errorContent: allErrorYaml {
      ...ErrorContentFragment
    }
  }
`;

const mapStateToProps = (state: { core: any }) => {
  return {};
};

const mapDispatchToProps = (dispatch: any) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ErrorPage as any);
