import React, { useEffect, FunctionComponent } from 'react';
import { graphql } from 'gatsby';
import { connect } from 'react-redux';
import { Router } from '@reach/router';
import { Page, Design, Section, Block } from '@liontechnyc/gemini';
import { placeholder, reduceGqlConnection } from '../utils';
import { SEO } from '../ui/components';
import { CoreCtx } from '../interfaces';
import { client } from '../api';

const design = {
  layout: [['menu', 'content']],
  grid: {
    y: ['1fr'],
    x: ['305px', 'auto'],
  },
  gutter: 0,
};

export interface AppProps {
  isLoggedIn: boolean;
}

const App: FunctionComponent<StaticPageProps & AppProps> = ({
  data: seoData,
  isLoggedIn,
}) => {
  const { seoContent } = seoData;
  const [{ meta }] = reduceGqlConnection(seoContent);

  useEffect(() => {
    if (!isLoggedIn && !client.accessToken) {
      console.warn('User is not authenticated!');
      window.location.replace('/logout/');
    }
  }, [isLoggedIn]);

  return (
    <Page>
      <SEO
        lang="en"
        metaTags={[{ name: 'robots', content: 'index,follow' }]}
        title="App"
        {...meta}
      />
      <Block className="app app__container">
        <Block fluid={true} className="app__container--wrapper">
          <Design is="view" containerClass="app__container--layout" {...design}>
            <Section name="menu">{/* <Menu /> */}</Section>
            <Section
              name="content"
              containerStyle={{
                backgroundImage: `url(${placeholder(128, 128)})`,
              }}
            >
              <div className="app__banner">
                <figure onClick={() => window.open('#', '_BLANK')}>
                  <img height={32} src={placeholder(64, 64)} />
                </figure>
              </div>
              <div className="app__ctx">
                <Router id="router" basepath="/app"></Router>
              </div>
            </Section>
          </Design>
        </Block>
      </Block>
    </Page>
  );
};

const mapStateToProps = (state: { core: CoreCtx }) => {
  return {
    isLoggedIn: state.core.auth.isLoggedIn,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {};
};

export const pageContent = graphql`
  query IndexPageContent {
    seoContent: allSeoYaml {
      ...SeoMetaFragment
    }
    navContent: allNavYaml {
      ...NavContentFragment
    }
    menuContent: allMenuYaml {
      ...MenuContentFragment
    }
  }
`;

export default connect(mapStateToProps, mapDispatchToProps)(App as any);
