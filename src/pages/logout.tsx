import React, { useEffect, useReducer, FunctionComponent } from 'react';
import { connect } from 'react-redux';
import compose from 'lodash/fp/compose';
import { client } from '../api';
import { actions } from '../redux';
import { CoreCtx } from '../interfaces';
import { Loading } from '../ui/components';

export interface LogoutPageProps {
  isLoggedIn: boolean;
  logout: () => void;
}

const LogoutPage: FunctionComponent<StaticPageProps & LogoutPageProps> = ({
  logout,
  isLoggedIn,
}) => {
  const [loading, stopLoading] = useReducer((state: boolean) => false, true);
  useEffect(logout, []);

  useEffect(() => {
    if (!isLoggedIn) {
      stopLoading();
    }
  }, [isLoggedIn]);

  useEffect(() => {
    if (!loading && !isLoggedIn) {
      window.location.replace('/');
    }
  }, [loading, isLoggedIn]);

  return <Loading />;
};

const mapStateToProps = (state: { core: CoreCtx }) => {
  const { core } = state;
  return {
    isLoggedIn: core.auth.isLoggedIn && !!client.accessToken,
  };
};

const mapDispatchToProps = (dispatch: (arg: any) => any) => {
  return {
    logout: () => {
      compose(dispatch, actions.logout)();
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LogoutPage as any);
