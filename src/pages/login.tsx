import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { graphql } from 'gatsby';
import { connect } from 'react-redux';
import { Page, Content, Block } from '@liontechnyc/gemini';
import compose from 'lodash/fp/compose';
import { placeholder, reduceGqlConnection } from '../utils';
import { CoreCtx } from '../interfaces';
import { actions } from '../redux';
import { client } from '../api';
import { SEO } from '../ui/components';

const PageContainer = styled(Block)`
  position: absolute;
  height: 100vh;
  width: 100vw;
  padding: 25px;
`;

export interface LoginPageProps {
  login: (e: string, p: string) => void;
  isLoggedIn: boolean;
}

const LoginPage: FunctionComponent<StaticPageProps & LoginPageProps> = ({
  data: { seoContent, loginContent },
  login,
}) => {
  const [{ meta }] = reduceGqlConnection(seoContent);
  const [{ content }] = reduceGqlConnection(loginContent);

  return (
    <Page>
      <SEO
        lang="en"
        metaTags={[{ name: 'robots', content: 'index,follow' }]}
        title="App Login"
        {...meta}
      />
      <PageContainer
        className="login__page"
        centered={true}
        style={{ background: `url(${placeholder(128, 128)})` }}
      >
        <Block className="login__container animate__animated animate__fadeIn">
          <a className="login__brand" href="#">
            <figure>
              <img src={placeholder(64, 64)} />
            </figure>
          </a>
          <Content
            containerClass="login__header"
            alignTitle="center"
            alignContent="center"
            {...content}
          />
        </Block>
      </PageContainer>
    </Page>
  );
};

const mapStateToProps = (state: { core: CoreCtx }) => {
  const { core } = state;
  return {
    isLoggedIn: core.auth.isLoggedIn && !!client.accessToken,
  };
};

const mapDispatchToProps = (dispatch: (arg: any) => any) => {
  return {
    login: (email: string, password: string) =>
      compose(dispatch, actions.login)(email, password),
  };
};

export const pageContent = graphql`
  query LoginPageContent {
    seoContent: allSeoYaml {
      ...SeoMetaFragment
    }
    loginContent: allLoginYaml {
      ...LoginContentFragment
    }
  }
`;

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage as any);
