import React, { FunctionComponent } from 'react';
import { graphql } from 'gatsby';
import { Page } from '@liontechnyc/gemini';
import { SEO } from '../ui/components';
import { reduceGqlConnection } from '../utils';

export const pageContent = graphql`
  query NotFoundPageContent {
    seoContent: allSeoYaml {
      ...SeoMetaFragment
    }
    navContent: allNavYaml {
      ...NavContentFragment
    }
  }
`;

const design = {
  layout: [['content'], ['footer']],
  grid: {
    y: ['1fr', 'auto'],
    x: ['1fr'],
  },
};

const NotFoundPage: FunctionComponent<StaticPageProps> = ({
  data: { seoContent, navContent },
}) => {
  const [{ meta }] = reduceGqlConnection(seoContent);

  return (
    <Page>
      <SEO
        lang="en"
        metaTags={[{ name: 'robots', content: 'index,follow' }]}
        title="404 Page Not Found"
        {...meta}
      />
    </Page>
  );
};

export default NotFoundPage;
