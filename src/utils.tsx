import React, { useEffect, useRef } from 'react';
import { StaticQuery } from 'gatsby';
import { AxiosResponse } from 'axios';
import { notificationsMiddleware } from './notifications';

// ? Redux middleware chain

export const appMiddlewares = [notificationsMiddleware];

// ? UI Utilites

/** Declarative setInterval */
export function useInterval(cb: () => void, delay?: number): void {
  const savedCallback: any = useRef();

  useEffect(
    function storeLatestCallback() {
      savedCallback.current = cb;
    },
    [cb]
  );

  useEffect(
    function setupIntervalTimer() {
      function tick() {
        savedCallback.current();
      }
      if (delay !== null) {
        const id = setInterval(tick, delay);
        return () => clearInterval(id);
      }
    },
    [delay]
  );
}

/** Inject page with static content during SSR */
export const withStaticQuery = (query: any) => {
  return (WrappedComponent: React.ElementType) => {
    return (props: any) => (
      <StaticQuery
        query={query}
        render={(data) => {
          return <WrappedComponent {...data} {...props} />;
        }}
      />
    );
  };
};

/** Convert GraphQL Relay Connection to native array-object pattern */
export function reduceGqlConnection(connection: { edges: [{ node: any }] }) {
  return connection.edges.map((edge) => edge.node);
}

export function reduceApiResults(result: AxiosResponse) {
  return result.data.payload;
}

/** Placeholder Image API factory */
export function placeholder(width: number, height: number): string {
  return `https://via.placeholder.com/${width}x${height}`;
}

// ? Formatting Utilites

export function formatCurrency(value: number, truncated?: boolean): string {
  const commaFormatRegex = /\B(?=(\d{3})+(?!\d))/g;
  const amount = value.toFixed(2).toString();
  const currencyString = '$ ' + amount.replace(commaFormatRegex, ',');
  return truncated
    ? (currencyString.split('.').shift() as string)
    : currencyString;
}

export const mapCardinalIndexToMonth: { [cardinalIndex: number]: string } = {
  [1]: 'January',
  [2]: 'February',
  [3]: 'March',
  [4]: 'April',
  [5]: 'May',
  [6]: 'June',
  [7]: 'July',
  [8]: 'August',
  [9]: 'September',
  [10]: 'October',
  [11]: 'November',
  [12]: 'December',
};

// Data Processing

export function arrayToBase64(content: number[]): string {
  return window.btoa(
    new Uint8Array(content).reduce(
      (data, byte) => data + String.fromCharCode(byte),
      ''
    )
  );
}

export function toTitleCase(str: string): string {
  return str.replace(/\w\S*/g, function (txt: string) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}

export function byLastModified(a: any, b: any) {
  return +new Date(b.updatedAt) - +new Date(a.updatedAt);
}

export function indexTypedArray(a: { [key: string]: any }[], i: string) {
  return a.reduce((dict: { [index: string]: any }, item) => {
    return i in item ? { ...dict, [item[i]]: item } : dict;
  }, {});
}

/** Used to eliminate boilerplate in handler functions */
export function normalizeEvent(e: DragEvent) {
  e.preventDefault();
  e.stopPropagation();
}

export function datesAreOnSameDay(first: Date, second: Date) {
  return (
    first.getFullYear() === second.getFullYear() &&
    first.getMonth() === second.getMonth() &&
    first.getDate() === second.getDate()
  );
}

/** Merge State w/o mutations/side effects */
export const merge = (state) => (delta) => {
  const oldState = { ...state };
  return { ...oldState, ...delta };
};
