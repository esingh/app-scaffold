import React from 'react';
import { graphql } from 'gatsby';
import { Page, Design, Section, Content } from '@liontechnyc/gemini';
import { SEO, Footer } from '../ui/components';
import { reduceGqlConnection } from '../utils';
import staticImages from '../images';
import './policy-template.scss';

export const pageContent = graphql`
  query PolicyPageContent($path: String!) {
    seoContent: allSeoYaml {
      ...SeoMetaFragment
    }
    navContent: allNavYaml {
      ...NavContentFragment
    }
    markdownContent: markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        path
        title
      }
    }
  }
`;
const design = {
  layout: [['content'], ['footer']],
  grid: {
    y: ['1fr', 'auto'],
    x: ['1fr'],
  },
};

const Template = (props: StaticPageProps) => {
  return <Page />;
};

export default Template;
